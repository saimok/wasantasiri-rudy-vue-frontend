import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    googleKey: "AIzaSyClkPiU5Ef17wBmoiuq94e4cvMsUvZqO6M",
    keywords: "บางซื่อ",
    jdata: null
  },
  mutations: {
    setGoogleKey(state, googleKey) {
      state.keywords = googleKey;
    },
    setKeywords(state, keywords) {
      state.keywords = keywords;
    },
    setJdata(state, jdata) {
      state.jdata = jdata;
    }
  },
  getters: {
    keywords: state => state.keywords,
    googleKey: state => state.googleKey,
    jdata: state => state.jdata
  }
  // actions: {}
});
