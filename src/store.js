import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    googleKey: "AIzaSyClkPiU5Ef17wBmoiuq94e4cvMsUvZqO6M",
    keywords: "บางซื่อ",
    jdata: null,
    location: "",
    places: null,
    loading: false
  },
  getters: {
    keywords: state => state.keywords,
    googleKey: state => state.googleKey,
    jdata: state => state.jdata,
    location: state => state.location,
    places: state => state.places,
    loading: state => state.loading
  },
  mutations: {
    setGoogleKey(state, googleKey) {
      state.keywords = googleKey;
    },
    setKeywords(state, keywords) {
      state.keywords = keywords;
    },
    setJdata(state, jdata) {
      state.jdata = jdata;
    },
    setLocation(state, data) {
      state.location = data;
    },
    setPlaces(state, data) {
      state.places = data;
    },
    setLoading(state, value) {
      state.loading = value;
    }
  }
});
