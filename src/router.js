import Vue from "vue";
import Router from "vue-router";
import Restaurantes from "./views/Restaurantes.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "restaurants",
      component: Restaurantes
    }
  ]
});
